package com.rwawrzyniak.securephotos.core.android

interface ShouldSkipAppCodeListener {
	fun shouldSkipAppCode(): Boolean = false
}
