package com.rwawrzyniak.securephotos.core.android

interface OnBackPressedListener {
	fun ignoreBackPress(): Boolean = false
}
