package com.rwawrzyniak.securephotos.ui.main.previewphotos.datasource.mapper

import android.graphics.Bitmap

data class ImageModel(val title: String, val bitmap: Bitmap)
