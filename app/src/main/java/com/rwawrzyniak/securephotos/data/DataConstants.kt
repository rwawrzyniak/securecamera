package com.rwawrzyniak.securephotos.data

object DataConstants {
	const val THUMBNAIL = "THUMBNAIL"
	const val THUMBNAIL_WIDTH_HEIGHT = 640
}
