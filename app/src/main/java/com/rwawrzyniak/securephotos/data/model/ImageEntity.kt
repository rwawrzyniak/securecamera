package com.rwawrzyniak.securephotos.data.model

data class ImageEntity(val title: String, val byteArray: ByteArray)
